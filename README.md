## Open Web Cats Questions

Where the [webcats.gitlab.io git repository](https://gitlab.com/web-cats/web-cats.gitlab.io)
is for the deployed web site at [webcats.gitlab.io](https://webcats.gitlab.io/),
this repository could be a place to list open questions and host discussions on 
web related category theory questions and answers.

See the [Issues](https://gitlab.com/web-cats/CG/issues) Database.

We are still working on what the right process should be, so please leave some
thoughts there.
